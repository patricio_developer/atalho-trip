package com.rentcar.atalhotrip.Callback;

import com.rentcar.atalhotrip.Model.DriverGeoModel;

public interface IFirebaseDriverInfoListener {
    void onDriverInfoLoadSuccess(DriverGeoModel driverGeoModel);
}
