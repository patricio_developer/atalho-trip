package com.rentcar.atalhotrip.Callback;

public interface IFirebaseFailedListener {
    void onFirebaseLoadFailed(String message);
}
